# About AI

## 两种学习方式

`监督学习`类似于`智力复制转移`，基于现有`labeled data`，资源越多、越好，学得的智慧越多

> `人类的智力`也逃不过这个规律。你只需设想 同一个人，把他`放在3000年前`和`放在3000年后`智力会有怎样的区别。

而`强化学习`基于经验的积累，属于无中生有，即使没有好`data`，只要处于一个`有规律的世界或者环境`，就能通过`不断地实验`获得智力

> 改变世界的科技创新，都是无中生有，靠实验出来的

## Two ways of learning

`Supervised learning` is more like `intelligence transferring`. It based on `existed labeled data`, the more resource you get, the more intelligent you'll be.&#x20;

But `reinforcement learning` is different, wisdom comes from none. Even if you don't have good data, as long as you are in `a regular world that was ruled by natural`, you can get wisdom or intelligence by `experiments`.

## General thinking about machine learning

Machine Learning is all about try\_and\_cache. (Try action, get whether that action is good or bad to reach a goal)

First, it has some basic input data and actions. It trys to memory the base input data to action pairs.

Then if single data element is not enough to overcome the difficultys, it will try to memory the input data sequence to action pairs.

The input sequence or list length will be bigger and bigger until it overcome the difficultys.

> The interesting part about human thinking is that sometimes they'll do abstriction to use short\_id to replace long input sequence, so that they don't have to remember a lot of long sequence but a few special examples.

And sometimes it will meet unknow long sequence input data, in this case, it will try to use random small length sequence data to perform actions.

If somehow it uses random sub\_sequence data win a success, it will remember those sub\_sequence data as a long sequence data.

But if his/her memory is about to run out, he/she will try to use sub\_sequence to reach the same result, so that it won't take too much memory and computing resources.

In this case, those small rules become `common sense` or `basic rules`.
